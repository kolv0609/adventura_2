
package cz.vse.kolv.logika;

import cz.vse.kolv.logika.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


/*******************************************************************************
 * Testovací třída BatohTest slouží ke komplexnímu otestování
 * třídy Batoh.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class BatohTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    /***************************************************************************
     * Testuje správnost fungování batohu. Lze vkládat a odebírat věci z něj, 
     * testuje i funkčnost kapacity batohu. Nelze vkládat více než 3 věci.
     */
    @Test
    public void testPocetMistVBatohu()
    {
        Vec vec1 = new Vec("prvni",true);
        Batoh batoh = new Batoh();
        assertEquals(true, batoh.jeMisto());
        Vec vec2 = new Vec("druhy", true);
        Vec vec3 = new Vec("treti", true);
        assertEquals(true, batoh.pridejDoBatohu(vec1));
        assertEquals(true, batoh.pridejDoBatohu(vec2));
        assertEquals(true, batoh.pridejDoBatohu(vec3));
        assertEquals(true, batoh.obsahujeVec("prvni"));
        assertEquals(false, batoh.jeMisto());
        Vec vec4 = new Vec("ctvrta", true);
        assertEquals(false, batoh.pridejDoBatohu(vec4));
        
        assertEquals(true, batoh.odebrat("prvni"));
        
        assertEquals(false, batoh.obsahujeVec("prvni"));
        
        assertEquals(true, batoh.odebrat("druhy"));
        
        assertEquals(true, batoh.jeMisto());
        Vec vec5 = new Vec("nelze", false);
        assertEquals(false, batoh.pridejDoBatohu(vec5));
        
    }

}
