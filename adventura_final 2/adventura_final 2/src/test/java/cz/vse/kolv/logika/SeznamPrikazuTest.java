
package cz.vse.kolv.logika;

import cz.vse.kolv.logika.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/*******************************************************************************
 * Testovací třída SeznamPrikazuTest slouží ke komplexnímu otestování
 * třídy SeznamPrikazu.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class SeznamPrikazuTest
{
    private Hra hra;
    private SeznamPrikazu platnePrikazy;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;
    private PrikazNapoveda prNapoveda;
    private PrikazSeber prSeber;
    private PrikazPoloz prPoloz;
    private PrikazOdemkni prOdemkni;
    private PrikazRozrizni prRozrizni;
    private PrikazSnez prSnez;
    
     
    
    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
        hra = new Hra();
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan());
        prNapoveda = new PrikazNapoveda(platnePrikazy);
        prSeber = new PrikazSeber(hra.getHerniPlan());
        prOdemkni = new PrikazOdemkni(hra.getHerniPlan());
        prRozrizni = new PrikazRozrizni(hra.getHerniPlan());
        prSnez = new PrikazSnez(hra.getHerniPlan());
        prPoloz = new PrikazPoloz(hra.getHerniPlan());
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @Test
    public void testVlozeni()
    {
        SeznamPrikazu seznam = new SeznamPrikazu();
        seznam.pridejPrikaz(prKonec);
        seznam.pridejPrikaz(prJdi);
        seznam.pridejPrikaz(prNapoveda);
        seznam.pridejPrikaz(prSeber);
        seznam.pridejPrikaz(prOdemkni);
        seznam.pridejPrikaz(prPoloz);
        seznam.pridejPrikaz(prRozrizni);
        seznam.pridejPrikaz(prSnez);
        
        assertEquals(prKonec, seznam.vratPrikaz("konec"));
        assertEquals(prJdi, seznam.vratPrikaz("jdi"));
        assertEquals(prNapoveda, seznam.vratPrikaz("napoveda"));
        assertEquals(prSeber, seznam.vratPrikaz("seber"));
        assertEquals(prOdemkni, seznam.vratPrikaz("odemkni"));
        assertEquals(prPoloz, seznam.vratPrikaz("poloz"));
        assertEquals(prSnez, seznam.vratPrikaz("snez"));
        assertEquals(prRozrizni, seznam.vratPrikaz("rozrizni"));
    }
    
    @Test
    public void testJePlatnyPrikaz()
    {
        SeznamPrikazu seznam = new SeznamPrikazu();
        seznam.pridejPrikaz(prKonec);
        seznam.pridejPrikaz(prJdi);
        seznam.pridejPrikaz(prNapoveda);
        seznam.pridejPrikaz(prSeber);
        seznam.pridejPrikaz(prOdemkni);
        seznam.pridejPrikaz(prPoloz);
        seznam.pridejPrikaz(prSnez);
        seznam.pridejPrikaz(prRozrizni);
        
        assertEquals(true, seznam.platnyPrikaz("konec"));
        assertEquals(true, seznam.platnyPrikaz("jdi"));
        assertEquals(true, seznam.platnyPrikaz("napoveda"));
        assertEquals(true, seznam.platnyPrikaz("seber"));
        assertEquals(true, seznam.platnyPrikaz("odemkni"));
        assertEquals(true, seznam.platnyPrikaz("poloz"));
        assertEquals(true, seznam.platnyPrikaz("snez"));
        assertEquals(true, seznam.platnyPrikaz("rozrizni"));
        
        assertEquals(false, seznam.platnyPrikaz("Konec"));
        assertEquals(false, seznam.platnyPrikaz("Jdi"));
        assertEquals(false, seznam.platnyPrikaz("Napoveda"));
        assertEquals(false, seznam.platnyPrikaz("Seber"));
        assertEquals(false, seznam.platnyPrikaz("Odemkni"));
        assertEquals(false, seznam.platnyPrikaz("Poloz"));
        assertEquals(false, seznam.platnyPrikaz("Snez"));
        assertEquals(false, seznam.platnyPrikaz("Rozrizni"));
    }
    
    /***************************************************************************
     * Test of the {@link #setUp()} method preparing the test fixture.
     */
    @Test
    public void testNazvyPrikazu()
    {
        SeznamPrikazu seznam = new SeznamPrikazu();
        seznam.pridejPrikaz(prKonec);
        seznam.pridejPrikaz(prJdi);
        seznam.pridejPrikaz(prNapoveda);
        seznam.pridejPrikaz(prSeber);
        seznam.pridejPrikaz(prOdemkni);
        seznam.pridejPrikaz(prPoloz);
        seznam.pridejPrikaz(prSnez);
        seznam.pridejPrikaz(prRozrizni);
        
        String nazev = seznam.nazvyPrikazu();
        assertEquals(true, nazev.contains("konec"));
        assertEquals(true, nazev.contains("jdi"));
        assertEquals(true, nazev.contains("napoveda"));
        assertEquals(true, nazev.contains("seber"));
        assertEquals(true, nazev.contains("odemkni"));
        assertEquals(true, nazev.contains("poloz"));
        assertEquals(true, nazev.contains("snez"));
        assertEquals(true, nazev.contains("rozrizni"));
    }

}
