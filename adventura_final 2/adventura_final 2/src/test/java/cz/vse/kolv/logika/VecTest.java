
package cz.vse.kolv.logika;

import cz.vse.kolv.logika.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/*******************************************************************************
 * Testovací třída VecTest slouží ke komplexnímu otestování
 * třídy Vec.
 *
 * @author Vít Kollarczyk
 * @version 1.0
 */
public class VecTest
{

   /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    /***************************************************************************
     * Testuje vkládání věci do místnosti. Testuje také zda-li lze věc z místnosti 
     * odebrat v případě, že je věc přenositelná.
     */
    @Test
    public void testVeci()
    {
        Mistnost pokoj = new Mistnost(null,  null);
        Vec vec1 = new Vec("lze", true);
        Vec vec2 = new Vec("nelze", false);
        pokoj.pridejVec(vec1);
        pokoj.pridejVec(vec2);
        assertEquals(true, pokoj.nachaziSeVMistnosti("lze"));
        assertEquals(true, pokoj.nachaziSeVMistnosti("nelze"));
        assertEquals(false, pokoj.nachaziSeVMistnosti("dalsi"));
        assertNotNull(pokoj.odeberVec("lze"));
        assertEquals(false, pokoj.nachaziSeVMistnosti("lze"));
        assertNull(pokoj.odeberVec("nelze"));
        assertEquals(true, pokoj.nachaziSeVMistnosti("nelze"));
        assertNull(pokoj.odeberVec("dalsi"));
        
    }

    @Test
    public void testVeciUvnitr()
    {
        Vec vec1 = new Vec("trezor",true);
        Vec vec2 = new Vec("hodinky", true);
        Vec vec3 = new Vec("dalsi", true);
                
        assertEquals(false, vec1.obsahuje("dalsi"));
        vec1.vlozDovnitr(vec3);
        assertEquals(false, vec1.obsahuje("dalsi"));
        vec1.setLzeOdemknout(true);
        vec1.vlozDovnitr(vec2);
        assertEquals(false, vec1.obsahuje("hodinky"));
        assertEquals(false, vec1.jeOdemcena());
        vec1.setJeOdemcena(true);
        assertEquals(true, vec1.obsahuje("hodinky"));
        assertEquals(false, vec1.obsahuje("dalsi"));
        vec1.vlozDovnitr(vec3);
        assertEquals(true, vec1.obsahuje("dalsi"));
        vec1.setJeOdemcena(false);
        assertEquals(false, vec1.obsahuje("dalsi"));
        assertNotNull(vec1.vratObsah());
    }
}
