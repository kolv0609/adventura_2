
package cz.vse.kolv.logika;


/*******************************************************************************
 * Třída PrikazKonec implementuje příkaz konec pro ukončení hry.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazKonec implements IPrikaz
{
    private static final String nazev = "konec";
    private Hra hra;
    
    /**
     * Vytvoří příkaz pro ukončení hry.
     * 
     * @param hra - Adventura
     */
    public PrikazKonec(Hra hra)
    {
        this.hra = hra;
    }
    
    /**
     * Spustí příkaz. Ověřuje správnost napsání příkazu, příkaz konec žádný parametr nemá, tudíž se zadává pouze jedno slovo.
     * 
     * @param parametry - hráč zadá název příkazu
     * @return vrací řetězec pokud se příkaz splní a hra se ukončí nebo pokud hráč zadal příkaz špatně.
     */
    @Override
    public String spustPrikaz(String... parametry)
    {
        if(parametry.length > 0)
        {
            return "Platný příkaz je pouze jedno slovo 'konec'";
        }
        else
        {
            hra.setKonecHry(true);
            return "Hra je ukončena.";
        }
    }
    
    /**
     * Vrací název příkazu.
     */
    @Override
    public String getNazev()
    {
        return nazev;
    }
}
