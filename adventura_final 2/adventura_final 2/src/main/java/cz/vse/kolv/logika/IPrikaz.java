
package cz.vse.kolv.logika;


/*******************************************************************************
 * Třída implementující toto rozhraní zpracovává ve hře jednotlivé příkazy.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
interface IPrikaz
{
    
    /**
     * Metoda pro provedení příkazu ve hře. Počet parametrů je závislý na konkrétním příkazu.
     * Např. příkaz konec parametr nemá, příkaz seber má jeden parametr.
     */
    public String spustPrikaz(String... parametry);
    
    /**
     * Vrací název příkazu.
     * 
     * @return nazev příkazu
     */
    public String getNazev();

}
