
package cz.vse.kolv.logika;


/*******************************************************************************
 * Třída PrikazSnez implementuje příkaz pro snězení věci, nacházející se uvnitř batohu.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazSnez implements IPrikaz
{
    private static final String nazev = "snez";
    private HerniPlan plan;
     
    
    /**
     * Vytvoří příkaz.
     * 
     * @param plan - herní plán s místnostmi a věcmi
     */
    public PrikazSnez(HerniPlan plan)
    {
        this.plan = plan;
    }
    
    /**
     * Spustí příkaz pro snězení věci. Ověřuje, zda-li se zadaná věc nachází v batohu a lze sníst.
     * 
     * @param - název věci, kterou hráč chce sníst
     * @return řetězec se zprávou pro hráče, zda li se zadanou věc podařilo sníst nebo ne.
     */
    @Override
    public String spustPrikaz(String... parametry)
    {
        String vyhodnoceni = "";
        if(parametry.length == 0)
        {
            vyhodnoceni = "Co mám sníst? Musís zadat název věci.";
        }
        else
        {
            String nazevVeci = parametry[0];
            Batoh batoh = plan.getBatoh();
            
            if(batoh.obsahujeVec(nazevVeci))
            {
                Vec bonbon = batoh.vratHodnotu(nazevVeci);
                if(bonbon.lzeSnist())
                {
                    if(batoh.obsahujeVec("bonbon"))
                    {
                    bonbon.setJeSneden(true);
                    batoh.odebrat("bonbon");
                    vyhodnoceni = "Snědl jsi bonbon";
                    }
                }
                else
                {
                    vyhodnoceni = "Toto nelze sníst";
                }
            }
            else
            {
                vyhodnoceni = "Zadaná věc se nenachází v batohu.";
            }
        }
        
        return vyhodnoceni;
    }

    /**
     * Vrací název příkazu.
     */
    @Override
    public String getNazev()
    {
        return nazev;
    }
}
