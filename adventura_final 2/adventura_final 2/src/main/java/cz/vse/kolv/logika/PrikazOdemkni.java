package cz.vse.kolv.logika;


/*******************************************************************************
 * Třída PrikazOdemkni implementuje příkaz pro odemykání věcí ve hře.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazOdemkni implements IPrikaz
{
    private static final String nazev = "odemkni";
    private HerniPlan plan;
    
    /**
     * Vytvoří příkaz. 
     * 
     * @param plan - herní plán s místnostmi a věcmi, které se v nich nachází.
     */
    public PrikazOdemkni(HerniPlan plan)
    {
        this.plan = plan;
    }

    /**
     * Spustí příkaz s parametrem názvu věci, která se má odemknout.
     * V případě, že hráčem zadaná věc se nachází v akuální místnosti, hráč má v batohu klíč a zadaná věc lze odemknout,
     * věc se odemkne a hráč tak získá přístup k věcem uvnitř. Využívá se pro věc trezor.
     * 
     * @param - hráčem zadaná věc, kterou chce odemknout
     * @return Vrací řetězec v závislosti na tom, zda-li se zadanou věc podařilo odemknout či nikoliv.
     * V případě, že ano, vrací se také věci, které se nachází uvnitř.
     */
    @Override
    public String spustPrikaz(String... parametry)
    {
        String vyhodnoceni = "";
        if(parametry.length == 0)
        {
            vyhodnoceni = "Nezadal jsi, co mám odemknout.";
        }
        else
        {
            String nazevTrezoru = parametry[0];
            Mistnost aktualni = plan.getAktualniMistnost();
            if(aktualni.obsahujeVec(nazevTrezoru))
            {
                Vec trezor = aktualni.vratVec(nazevTrezoru);
                
                if(trezor.jdeOdemknout())
                {
                    Batoh batoh = plan.getBatoh();
                    if(batoh.obsahujeVec("klíč"))
                    {
                        trezor.setJeOdemcena(true);
                        return "Odemkl jsi trezor, uvnitř se nachází: " + trezor.vratObsah();
                        
                    }
                    else
                    {
                        vyhodnoceni = "V batohu se nenachází klíč k odemknutí trezoru.";
                    }
                }
                else
                {
                    vyhodnoceni = "Toto nelze odemknout.";
                }
            }
            else
            {
                vyhodnoceni = "Zadaná věc se zde nenachází.";
            }
        }
        return vyhodnoceni;
    }
    
    /**
     * Vrací název příkazu.
     */
    @Override
   public String getNazev()
   {
       return nazev;
   } 
}
