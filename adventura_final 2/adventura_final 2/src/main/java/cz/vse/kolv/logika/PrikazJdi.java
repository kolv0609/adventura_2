
package cz.vse.kolv.logika;

/*******************************************************************************
 * Třída PrikazJdi implementuje příkaz jdi ve hře.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazJdi implements IPrikaz
{
    private static final String nazev = "jdi";
    private HerniPlan plan;
    
    /**
     * Vytvoří příkaz. 
     * 
     * @param plan - herní plán s jednotlivými místnostmi ve hře.
     */
    public PrikazJdi(HerniPlan plan)
    {
        this.plan = plan;
    }
    
    /**
     * Metoda spustí příkaz. Hráč zadá název místnosti, do které chce vstoupit.
     * Dále se vyhodnotí, zda-li je zadaná místnost v seznamu sousedních místností v aktuální místnosti.
     * Pokud zadaná místnost v seznamu je, hráč do ní vstoupí.
     * 
     * @param parametry - hráč zadává název místnosti, do které chce vstoupit
     * @return vrací popis místnosti, do které hráč vstupuje. V případě, že do ní vstoupit nelze, 
     * vrací řetězec s chybovou hláškou.
     * 
     */
    @Override
    public String spustPrikaz(String... parametry)
    {
        if(parametry.length == 0)
        {
            return "Zadej název místnosti, do které mám vstoupit.";
        }
        
        String urceni = parametry[0];
        Mistnost sousedniMistnost = plan.getAktualniMistnost().napisSousedniMistnost(urceni);
        
        if(sousedniMistnost == null)
        {
            return "Do zadané místnosti nelze z tohoto pokoje vstoupit!";
        }
        else
        {
            plan.setAktualniMistnost(sousedniMistnost);
            return sousedniMistnost.dlouhyPopis();
        }
    }
    
    /**
     * Vrací název příkazu.
     * 
     * @return nazev příkazu
     */
    @Override
    public String getNazev()
    {
        return nazev;
    }
}
