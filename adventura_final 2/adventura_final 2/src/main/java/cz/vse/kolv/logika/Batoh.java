
package cz.vse.kolv.logika;

import java.util.Map;
import java.util.HashMap;

/*******************************************************************************
 * Třída Batoh umožňuje vytvořit batoh, který ve hře složí k přenášení jednotlivých dárků.
 * Má omezenou kapacitu, hráč tak nemůže sbírat všechny věci najednou. Některé věci z batohu lze dále použít,
 * např. klíč k odemčení trezoru nebo bonbon ke snězení. Z batohu lze všechny věci v libovolné místnosti 
 * vyndat.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class Batoh
{
    private Map<String,Vec> obsahBatohu;   
    private static final int misto = 3;
    
    /**
     *Konstruktor pro vytvoření batohu.
     */
    public Batoh()     
    {
        
        obsahBatohu = new HashMap<>();    
        
    }
    
    /**
     * Vypíše řetězec včetně seznamu věcí, které se aktuálně nachází v batohu.
     */
    public String dlouhyPopis()
    {
        return "V batohu se nachází: " + seznamVeci();
    }
    
    /**
     * Vrací seznam věcí v batohu.
     */
    public String seznamVeci()
    {
        String seznam = "";
        
        for(String neco : obsahBatohu.keySet())     
        {
            
            seznam += " " + neco;
        }
        return seznam;
    }
    
    /**
     * Meotda ověřující, zda-li se zadaná věc nachází v batohu.
     * 
     * @param nazevVeci - název věci, o které zjišťuji, jestli se nachází v batohu
     * @return - true v případě, že se zadaná věc nachází v batohu, jinak false
     */
    public boolean obsahujeVec(String nazevVeci)
    {
        for (String neco : obsahBatohu.keySet())           //Vec neco : obsahBatohu
        {
           
           if(neco.equals(nazevVeci))
           {
               return true;
           }
        }
        return false;
    }
    
    /**
     * Metoda pro zjištění, jestli je ještě volné místo v batohu. Metoda se využívá při 
     * vkládání nové věci do batohu. Pokud metoda vrátí false, nelze do batohu vložit novou věc.
     * 
     * @return true pokud je obsah batohu menší než dostupná kapacita, jinak false
     */
    public boolean jeMisto()
    {
        return (obsahBatohu.size() < misto);
    }
    
    /**
     * Vložení věci do batohu. Pokud lze zadanou věc sebrat a zároveň je v batohu volné místo,
     * proběhne vložení v pořádku. 
     * 
     * @param neco - věc, kterou chci vložit do batohu
     * @return true pokud vložení proběhne v pořádku, jinak false
     */
    public boolean pridejDoBatohu(Vec neco)
    {
        if(jeMisto() && neco.lzeSebrat())
        {
            obsahBatohu.put(neco.getNazev(), neco);
            return true;
        }  
        else
        {
        return false;
        }
    }
    
    /**
     * Metoda vrací název konkrétní jedné věci. Využívá se např. při příkazu sněz nebo polož, 
     * kdy v batohu hledám název jedné konkrétní věci.
     * 
     * @param nazevVeci - název věci, kterou v batohu hledám
     * @return hodnota - název hledané věci
     */
    public Vec vratHodnotu(String nazevVeci)
    {
        Vec hodnota = null;
        
        if(obsahBatohu.containsKey(nazevVeci))
        {
            hodnota = obsahBatohu.get(nazevVeci);
        }
        
        return hodnota;
    }
    
    /**
     * Odebírá konkrétní věc z batohu. Nejprve ověří, zda-li se skutečně zadaná věc nachází v batohu.
     * 
     * @param nazevVeci - název konkrétní věci, kterou chci z batohu odebrat
     * @return true, pokud se věc nachází v batohu. Zadaná věc se úspěšně odebere a uvolní se tak místo v batohu
     *         false, v případě, že se zadaná věc v batohu nenachází
     */
    public boolean odebrat(String nazevVeci)
    {
        if(obsahujeVec(nazevVeci))
        {
            obsahBatohu.remove(nazevVeci);
            return true;
        }
        else
        {
            return false;
        }
    }
    
}
