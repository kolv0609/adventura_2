
package cz.vse.kolv.logika;


/**
 * Rozhraní, které hra musí implementovat. Váže se na ně uživatelské rozhraní.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public interface IHra
{
    /**
     * Vrátí text - úvod hry.
     */
    public String vratUvod();
    
    /**
     * Vrací závěrečnou zprávu hry.
     */
    public String vratZaver();
    
    /**
     * Vrací hodnotu true v případě, že hra skončila a hodnotu false v případě, že hra dále běží.
     */
    public boolean konecHry();
    
    /**
     * Metoda pro zpracování zadaných příkazů. Rozdělí slovo příkazu na samotné slovo příkazu a další parametry.
     * Dále testuje zda je zadaný příkaz mezi platnými příkazy. V případě, že ano, spustí se samotný příkaz.
     * 
     * @param radek - zadává uživatel ve formě příkazu během hry.
     * @return - vypíše řetězec 
     */
    public String spustPrikaz(String radek);
    
    /**
     * Metoda odkazuje na vytvořený herní plán, třída HerniPlan.
     *
     *@return odkaz na herní plán
     */
    public HerniPlan getHerniPlan();
}
