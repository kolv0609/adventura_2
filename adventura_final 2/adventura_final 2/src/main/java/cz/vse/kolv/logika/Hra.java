
package cz.vse.kolv.logika;

/*******************************************************************************
 * Třída Hra - představuje logiku hry.
 * Jedná se o hlavní třídu, která vytváří instanci třídy HerniPlan, inicializující místnosti.
 * Zároveň vytváří seznam příkazů, které lze během hry použít.
 * Dále vypisuje úvod a závěr hry.
 * 
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class Hra implements IHra
{
    private SeznamPrikazu prikazy;
    private HerniPlan herniPlan;
    private boolean konecHry = false;
        
    /**
     * Konstruktor pro vytvoření hry, příkazů a prostřednictvím třídy HerniPlan vytvoří místnosti.
     */
    public Hra()
    {
        herniPlan = new HerniPlan();
        prikazy = new SeznamPrikazu();
        prikazy.pridejPrikaz(new PrikazNapoveda(prikazy));  
        prikazy.pridejPrikaz(new PrikazJdi(herniPlan));
        prikazy.pridejPrikaz(new PrikazSeber(herniPlan));
        prikazy.pridejPrikaz(new PrikazKonec(this));
        prikazy.pridejPrikaz(new PrikazOdemkni(herniPlan));
        prikazy.pridejPrikaz(new PrikazRozrizni(herniPlan));
        prikazy.pridejPrikaz(new PrikazSnez(herniPlan));
        prikazy.pridejPrikaz(new PrikazPoloz(herniPlan));
        prikazy.pridejPrikaz(new PrikazOtevri(herniPlan));
        prikazy.pridejPrikaz(new PrikazProjdi(herniPlan));
    }    
    
    /**
     * Vrací uvítací text.
     */
    public String vratUvod()
    {
        return "Vítejte ve hře! \n" + "Santa Claus rozmístil po domě dárky. \n" + "Zvládnete je najít všechny? \n" 
        + "Napište 'napoveda' pro získání nápovědy.\n" + "\n" +        
        herniPlan.getAktualniMistnost().dlouhyPopis();
    }
       
    /**
     * Vrací závěrečný text.
     */
    public String vratZaver()
    {
        return "Konec hry.";
    }        
    
    /**
     * Vrací hodnotu true / false v závislosti na tom, zda-li hra skončila.
     */
    public boolean konecHry()
    {
        return konecHry;
    }
    
    /**
     * Metoda pro zpracování zadaných příkazů. Rozdělí slovo příkazu na samotné slovo příkazu a další parametry.
     * Dále testuje zda je zadaný příkaz mezi platnými příkazy. V případě, že ano, spustí se samotný příkaz.
     * 
     * @param radek - zadává uživatel ve formě příkazu během hry.
     * @return - vypíše řetězec
     */
    public String spustPrikaz(String radek)
    {
        String [] slova = radek.split("[ \t]+");
        String nazevPrikazu = slova[0];
        String [] parametry = new String[slova.length -1];
        
        for(int i=0; i < parametry.length; i++)
        {
            parametry[i] = slova[i+1];
        }
        
        String textKVypsani = " .... ";
        
        if(prikazy.platnyPrikaz(nazevPrikazu))
        {
            IPrikaz prikaz = prikazy.vratPrikaz(nazevPrikazu);
            textKVypsani = prikaz.spustPrikaz(parametry);
            if(herniPlan.vyhra())
            {
                textKVypsani += "\n Gratuluji, vyhrál jsi!";
                konecHry = true;
            }
                        
        }
        else
        {
            textKVypsani = "Zadal jsi neplatný příkaz.";
        }
        return textKVypsani;
    } 
    
    /**
     * Nastavuje konec hry. Metodu využívá třída PrikazKonec.
     * 
     * @param konecHry - hodnota true ukončí hru, hodnota false umožní pokračování ve hře
     */
    void setKonecHry(boolean konecHry)
    {
        this.konecHry = konecHry;
    } 
    
    /**
     * Metoda odkazuje na vytvořený herní plán, třída HerniPlan.
     *
     *@return odkaz na herní plán
     */
    public HerniPlan getHerniPlan()
    {
        return herniPlan;
    }
}
