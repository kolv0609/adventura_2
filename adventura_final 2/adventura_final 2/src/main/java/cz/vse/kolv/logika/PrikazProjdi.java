
package cz.vse.kolv.logika;


/*******************************************************************************
 * Instance třídy {@code PrikazProjdi} představují ...
 * The {@code PrikazProjdi} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class PrikazProjdi implements IPrikaz
{
   private static final String nazev = "projdi";
   private HerniPlan plan;
    /***************************************************************************
     */
    public PrikazProjdi(HerniPlan plan)
    {
        this.plan = plan;
    }

    @Override
    public String spustPrikaz(String... parametry)
    {
        String vyhodnoceni = "";
        
        Mistnost pruchod = new Mistnost("Pruchod",", touto místností se dostaneš do výherního prostoru.");
        Mistnost vyhra = new Mistnost("Vyhra",", toto je výherní místnost, kam se lze dostat pouze skrze průchod.");
        pruchod.setVychod(vyhra);
        
        if(parametry.length == 0)
        {
            vyhodnoceni = "Nezadal jsi, co mám odemknout.";
        }
        else
        {
            String nazevPortalu = parametry[0];
            Mistnost aktualni = plan.getAktualniMistnost();
            if(aktualni.obsahujeVec(nazevPortalu))
            {
                Vec portal = aktualni.vratVec(nazevPortalu);
                Batoh batoh = plan.getBatoh();
                if(portal.lzeProjit() && batoh.obsahujeVec("klicek"))
                {
                    plan.setAktualniMistnost(pruchod);
                    vyhodnoceni = "Prošel jsi portálem. Nacházíš se v místnosti 'pruchod', kudy můžeš vstoupit do výherní místnosti.";
                }
                else
                {
                    vyhodnoceni = "Tudy nelze projít.";
                }
            }
            
            else
            {
                vyhodnoceni = "Zadaná věc se v místnosti nenachází.";
            }
        }
        return vyhodnoceni;
    }
    
    
     @Override
   public String getNazev()
   {
       return nazev;
   }
}
