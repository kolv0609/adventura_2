
package cz.vse.kolv.logika;


/*******************************************************************************
 * Instance třídy {@code PrikazOtevri} představují ...
 * The {@code PrikazOtevri} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class PrikazOtevri implements IPrikaz
{
    private static final String nazev = "otevri";
    private HerniPlan plan;
    /***************************************************************************
     */
    public PrikazOtevri(HerniPlan plan)
    {
        this.plan = plan;
    }

    @Override
    public String spustPrikaz(String... parametry)
    {
        String vyhodnoceni = "";
        if(parametry.length == 0)
        {
            vyhodnoceni = "Nezadal jsi, co mám odemknout.";
        }
        
        else
        {
            String nazevTruhly = parametry[0];
            Mistnost aktualni = plan.getAktualniMistnost();
            if(aktualni.obsahujeVec(nazevTruhly))
            {
                Vec truhla = aktualni.vratVec(nazevTruhly);
                
                if(truhla.lzeOtevrit())
                {
                    truhla.setJeOtevrena(true);
                    return "Otevřel jsi truhlu. Uvnitř se nachází: " + truhla.vratObsah();
                }
                else
                {
                    vyhodnoceni = "Toto nelze otevřít.";
                }
            }
            
            else
            {
                vyhodnoceni = "Zadaná věc se v místnosti nenachází.";
            }
        }
        return vyhodnoceni;
}

    /**
     * Vrací název příkazu.
     */
    @Override
   public String getNazev()
   {
       return nazev;
   }
}
