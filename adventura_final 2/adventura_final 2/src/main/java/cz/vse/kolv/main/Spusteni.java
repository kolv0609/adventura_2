
package cz.vse.kolv.main;

import cz.vse.kolv.logika.*;
import cz.vse.kolv.uiText.TextoveRozhrani;

/*******************************************************************************
 * Instance třídy {@code Spusteni} představují ...
 * The {@code Spusteni} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Spusteni
{
    
    /***************************************************************************
     */
    public static void main (String[] args)
    {
        IHra hra = new Hra();
        TextoveRozhrani ui = new TextoveRozhrani(hra);
        ui.hraj();
        
    }
}
