
package cz.vse.kolv.logika;

/*******************************************************************************
 * Třída PrikazPoloz implementuje příkaz pro vyndání věci z batohu.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazPoloz implements IPrikaz
{
    private static final String nazev = "poloz";
    private HerniPlan plan;
    /**
     * Vytvoří příkaz
     * 
     * @param plan - herní plán s místnostmi a věcmi
     */
    public PrikazPoloz(HerniPlan plan)
    {
        this.plan = plan;
    }
    
    /**
     * Spustí příkaz pro odebrání věci z batohu. Hráč zadá věc, kterou chce odebrat.
     * Následně se vyhodnotí, zda-li se věc nachází v batohu. Pokud ano, věc se z batohu odebere a zároveň se 
     * přidá do seznamu věci v aktuální místnosti, ve které se hráč nachází.
     * 
     * @param - nazev věci, kterou chce hráč odebrat z batohu
     * @return řetězec vrací zprávu hráči o tom, zda li se věc podařilo z batohu vyndat.
     */
    @Override
    public String spustPrikaz(String... parametry)
    {
        String vyhodnoceni = "";
        if(parametry.length == 0)
        {
            vyhodnoceni = "Nezadal jsi, co chceš vyndat z batohu.";
        }
        else
        {
            String nazevVeci = parametry[0];
            Mistnost aktualni = plan.getAktualniMistnost();
            Batoh batoh = plan.getBatoh();
            if(batoh.obsahujeVec(nazevVeci))
            {
                Vec polozena = batoh.vratHodnotu(nazevVeci);
                batoh.odebrat(nazevVeci);
                aktualni.pridejVec(polozena);
                vyhodnoceni = "Z batohu jsi vyndal " + nazevVeci;
            }
            else
            {
                vyhodnoceni = "Zadaná věc se nenachází v batohu.";
            }
        }
        return vyhodnoceni;
    }
    
    /**
     * Vrací název příkazu.
     */
    @Override
    public String getNazev()
    {
        return nazev;
    }
}






