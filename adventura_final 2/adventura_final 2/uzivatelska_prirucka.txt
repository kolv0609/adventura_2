U�IVATELSK� P��RU�KA K PROJEKTU ADVENTURA

Hra spo��v� ve sb�r�n� d�rk�, kter� Santa Claus p�es noc rozm�stil po dom�. Hr�� proch�z� jednotliv� m�stnosti 
v dom� a sb�r� v�ci, kter� lze sebrat. N�kter� v�ci pat�� mezi d�rky, jin� jsou pouze pomocn� a lze je
b�hem hry d�le pou��t (nap�. kl�� k odem�en� trezoru).

N�pov�da:
- mezi d�rky, kter� hr�� mus� sebrat do batohu pat��: telefon (v m�stnosti Chodba),
parf�m (v m�stnosti Koupelna) a hodinky, kter� jsou ulo�en� v trezoru v m�stnosti Pracovna.

- D�le hr�� mus� sn�st bonbon, kter� se nach�z� uvnit� ply�ov�ho medv�dka.

- Trezor lze odemknout pouze pokud m� hr�� v batohu kl��, kter� se nach�z� v Lo�nici.

- Medvidek lze roz��znout pouze pokud m� hr�� v batohu n��, kter� se nach�z� v Kuchyni.

P��kazy:
- napoveda - nap�e c�l hry, dostupn� p��kazy pro pou�it� ve h�e
- konec - po tomto p��kazu se hra ukon��
- jdi - p��kaz slou�� k proch�zen� mezi jednotliv�mi m�stnostmi. Zad�v� se ve form�: jdi 'nazev mistnosti'
- seber - sebere v�c, pokud se nach�z� v m�stnosti a lze ji sebrat. Vlo�� se do batohu. Ten 
m� v�ak omezenou kapacitu, a proto pokud je batoh pln�, nelze do n�j vlo�it dal�� v�c.
Zad�v� se ve form�: seber 'nazev v�ci'
- poloz - odeb�r� v�c z batohu a p�id� ji do aktu�ln�ho prostoru, ve kter�m se hr�� nach�z�.
Zad�v� se ve form�: poloz 'nazev v�ci'
- snez - p��kaz umo�n� sn�st v�c z batohu, pokud sn�st lze. V t�to h�e se jedn� o v�c 'bonbon'.
Po proveden� p��kazu se v�c z batohu sma�e. Zad�v� se: snez 'nazev v�ci'
- odemkni - p��kazem se odemyk� v�c v prostoru, pokud zadan� v�c lze odemknout. Ve h�e se jedn� o v�c 
trezor, kter� lze t�mto p��kazem odemknout a z�skat tak v�c uvnit�.
Zad�v� se formou: odemkni 'nazev v�ci'
- rozrizni - p��kazem se roz�e�e v�c v m�stnosti, pokud lze roz��znout. 
Ve h�e se jedn� o ply�ov�ho medv�dka, uvnit� kter�ho se
nach�z� bonbon. Zad�v� se formou: rozrizni 'nazev v�ci'

N�vod na pr�chod hry
Hra za��n� v m�stnosti P�eds��. Odtud se jde do Chodby, kde se nach�z� telefon. Ten se sebere do batohu. D�le
se jde do m�stnosti Lo�nice, odtud hr�� vezme kl��, kter�m se d� odemknout trezor. D�le Se pokra�uje do m�stnosti
Pracovna (p�es Chodbu a Ob�vac�_pokoj). V Pracovn� se nach�z� trezor, pou�it�m p��kazu odemkni trezor se trezor
odemkne. Hr�� sebere hodinky, kter� jsou v trezoru a polo�� kl��, jeliko� jej nebude d�le pot�ebovat.
D�le p�es Ob�vac�_pokoj hr�� vstoup� do Kuchyn�, kde vezme nuz. S no�em se vrac� do Ob�vac�ho_pokoje, d�le
do Chodby a vstupuje do m�stnosti D�tsk�_pokoj, kde se nach�z� ply�ov� medv�dek.
P��kazem rozrizni medvidek hr�� z�sk� mo�nost sebrat v�c, kter� se nach�z� uvnit� medv�dka.
Hr�� polo�� nuz, ten uz nebude d�le pot�eba a sebere bonbon, kter� je uvnit� medvidka.
Pou�it�m p��kazu snez bonbon hr�� sn� bonbon a uvoln� tak m�sto v batohu. 
D�le pokra�uje zp�t do Chodby a d�le do Koupelny, kde se nach�z� posledn� p�edm�t, kter� 
je nutn� sebrat. Hr�� sebere parf�m, ��m� se spln� podm�nka, �e v batohu se nach�z�
parf�m, telefon, hodinky a z�rove� je sn�den bonbon. 